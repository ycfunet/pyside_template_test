import sys
from PySide.QtGui import *
from PySide.QtCore import *

from main_widget import MainWidget

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    main_widget = MainWidget()
    main_widget.show()

    sys.exit(app.exec_())

