from PySide.QtGui import *
from PySide.QtCore import *

from ui_text_dialog import Ui_Dialog

# 參考：
# https://srinikom.github.io/pyside-docs/PySide/QtGui/QAbstractButton.html#signals

class MainWidget(QDialog):

    ui = Ui_Dialog()

    def __init__(self):
        QDialog.__init__(self)
        self.ui.setupUi(self)
        self.ui.CloseButton.setText("關閉視窗")

        self.ui.CloseButton.clicked.connect(self.on_CloseButton_clicked)

    def on_CloseButton_clicked(self):
        QApplication.exit(0)

